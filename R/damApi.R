# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

#' Get dam information by the dam code
#'
#' @export
#' @param damCode String - dam code
#' @param silent Boolean - log if true
#' @return Dam information
#' @examples
#' getDam("dam_code")
getDam = function(damCode, silent = FALSE) {
  checkLibraries()
  checkGlobalVariables()
  request <- paste(get("WIMES_URL", envir = .GlobalEnv), "/maestreaudb/api/v1/", 
                   get("WIMES_KEY", envir = .GlobalEnv), "/dam/", damCode, 
                   sep = "")
  
#   TODO JWI
#   if (!silent) {
#     cat(request, sep = "\n")
#   }
  
  apiResult <- requestAndParseJson(request)
  
  return(apiResult)
}

#' Get dam information by the sensor code
#'
#' @export
#' @param sensorCode String - sensor code
#' @param silent Boolean - log if true
#' @return Dam information
#' @examples
#' getDamBySensorCode("sensor_code")
getDamBySensorCode = function(sensorCode, silent = FALSE) {
  checkLibraries()
  checkGlobalVariables()
  request <- paste(get("WIMES_URL", envir = .GlobalEnv), "/maestreaudb/api/v1/", 
                   get("WIMES_KEY", envir = .GlobalEnv), "/dam/sensor/", sensorCode, 
                   sep = "")
  
#   TODO JWI
#   if (!silent) {
#     cat(request, sep = "\n")
#   }
  
  apiResult <- requestAndParseJson(request)
  
  return(apiResult)
}
