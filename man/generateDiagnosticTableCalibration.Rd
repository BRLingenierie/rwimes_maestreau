% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/graphicOutputs.R
\name{generateDiagnosticTableCalibration}
\alias{generateDiagnosticTableCalibration}
\title{Génère un tableau contenant les valeurs à l'extérieur de l'intervalle de confiance 95%/99% suite à un calage en PDF
Si plusieurs id de calages sont fournies, on concatène les tableaux au sein d'un seul PDF
Voir fichier d'exemple /examples_graphic_outputs/diag_analyse.pdf}
\usage{
generateDiagnosticTableCalibration(sensorCodes, filePath)
}
\arguments{
\item{sensorCodes}{String - sensor codes separated by ';'}

\item{filePath}{String - path where the pdf result is saved}
}
\value{
the path of the produced file, null if there is an error
}
\description{
Génère un tableau contenant les valeurs à l'extérieur de l'intervalle de confiance 95%/99% suite à un calage en PDF
Si plusieurs id de calages sont fournies, on concatène les tableaux au sein d'un seul PDF
Voir fichier d'exemple /examples_graphic_outputs/diag_analyse.pdf
}
\examples{
generateDiagnosticTableCalibration("MAE_SEN-DRRD;MAE_SEN-DRRG", "/home/rstudio/graphicOutputs/2018-02-12T23:45:00Z/fourChartsAnalysis.pdf")
}
