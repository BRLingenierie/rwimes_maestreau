% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/analysisDataGetting.R
\name{analysisTemperatureVariablesCalculation}
\alias{analysisTemperatureVariablesCalculation}
\title{temperature variable calculation - EDF method}
\usage{
analysisTemperatureVariablesCalculation(
  isCalibration,
  calibrationOrAnalysisId,
  temperatureTSCode,
  baseTimeT0,
  sensorMeasureValue,
  startDate,
  endDate,
  endDate_analysis
)
}
\arguments{
\item{isCalibration}{Boolean - true for calibration, false for analysis}

\item{calibrationOrAnalysisId}{String - calibration or analysis id}

\item{temperatureTSCode}{String - temperature TS code}

\item{baseTimeT0}{Double - base time T0 ajusted variable, in year}

\item{sensorMeasureValue}{table - sensorMeasureValue(date, value)}

\item{startDate}{Date (ex : as.POSIXct("2002-01-01 00:00:00", tz = "UTC"))}

\item{endDate}{Date (ex : as.POSIXct("2002-01-01 00:00:00", tz = "UTC"))}

\item{endDate_analysis}{Date (ex : as.POSIXct("2002-01-01 00:00:00", tz = "UTC"))}
}
\value{
Season variable values
}
\description{
temperature variable calculation - EDF method
}
