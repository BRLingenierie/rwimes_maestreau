\documentclass[10pt]{article}
\usepackage[T1]{fontenc}
\def\textunderscore{\char\_}

\usepackage{booktabs}

% \renewcommand{\familydefault}{\sfdefault}
%\usepackage[sfdefault]{Roboto}
%\usepackage[latin1]{inputenc}
% for encoding utf-8
\usepackage{ucs}
\usepackage[utf8x]{inputenc}

\usepackage{graphics}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{mdframed}

\usepackage{textpos}
\usepackage{multirow}
%\usepackage{SIunits}
\usepackage[labelformat = empty]{caption}
\usepackage[headsep=60pt]{geometry}
 \geometry{
 a4paper,
 left=10mm,
 right=10mm,
 top=32mm,
 bottom=50mm
 }
\usepackage{xcolor,colortbl} 
\definecolor{headColor}{HTML}{E6E6E6}

\usepackage{tabularx}
\usepackage{longtable}
\def\arraystretch{1.5}%  1 is the default, change whatever you need
\usepackage{array}

% header & footer
\graphicspath{{\Sexpr{filePath}}}
\usepackage{lastpage}
\usepackage{fancyhdr,color}
\pagestyle{fancy}
\fancyhf{}
\setlength\headheight{15pt}
\lhead{\Large\textsc{\Sexpr{print(damData$name)}}}
\rhead{Cote RN (m NGF) : \Sexpr{damData$normalWaterLevel}}
\chead{
    \begin{textblock}{14}(0, 0.5)
    %\vspace{5 mm}
    \begin{minipage}{\linewidth}
      \begin{mdframed}[backgroundcolor = headColor]
        \center{\bf\large{Fiches de diagnostic des mesures corrigées des effets réversibles (p. analyse)}}
      \end{mdframed}
    \end{minipage}%
    \end{textblock}%
    }
\cfoot{
      \begin{mdframed}[hidealllines = false]
        \center \footnotesize{Test de normalité : mesure "Ext. intervalle à 95\%" si l'écart au modèle est compris entre les intervalles de confiance à 95\% et 99\% de la population des écarts,"Ext. intervalle à 99\%" si il est situé au-delà de l'intervalle de confiance à 99\%.}
      \end{mdframed}%
      \vspace{-12pt}
      \begin{mdframed}[backgroundcolor = headColor, rightline = true, topline = true, leftline = true, bottomline = true]
        \center \footnotesize{\bf S'applique aux mesures des instruments dont le comportement modélisé est statistiquement significatif (R² > 0.75)}\\
      \end{mdframed}%
      \vspace{-7pt}
    \begin{minipage}{.175\textwidth}
      \includegraphics[width=\linewidth,height=30pt,keepaspectratio]{./../logo_brli.png}
      %\includegraphics[width=\linewidth,height=30pt,keepaspectratio]{\Sexpr{paste0("./../", damData$code, ".png")}}
      \includegraphics[width=\linewidth,height=27pt,keepaspectratio]{./../logo_Wmaestreau.png}
    \end{minipage}%
    \begin{minipage}{.825\textwidth}
      \parbox[b]{.8\textwidth}{%
      \center \footnotesize{Analysis id : \Sexpr{analysisId}/ \Sexpr{productionDateTime}}}\hfill
      \parbox[b]{.2\textwidth}{%
      \raggedleft \footnotesize{Page \thepage/\pageref{LastPage}}}\\
      \rule{\textwidth}{0.5pt}
    \end{minipage}%
    }


%-------------
\usepackage[noae,nogin]{Sweave}

\setkeys{Gin}{width=1\textwidth}


%-------------
\begin{document}

\SweaveOpts{concordance=TRUE}


%-------------
% 1stline
\begin{textblock}{3}(0,-0.25)
  \begin{minipage}{\linewidth}
    \begin{mdframed}[backgroundcolor = headColor, topline = false, rightline = false, leftline = false, bottomline = false]
      \center{\texttt{\detokenize{\Sexpr{sensorCode_1}}}}
    \end{mdframed}
  \end{minipage}
\end{textblock}

\begin{textblock}{12}(4.5,-0.2)
  \begin{minipage}{\linewidth}
      Coefficient de corrélation multiple déduit de la variance résiduelle: R\textsuperscript{2} : \Sexpr{round(coefReg$corelCoeff, 2)}
  \end{minipage}
\end{textblock}


%-------------
% 2nd line
\begin{textblock}{14}(0,0.25)
  \begin{minipage}{\linewidth}
    \parbox[t]{.2\textwidth}{\Sexpr{sensorData$sensorType} - \Sexpr{sensorData$unit}}
    \parbox[t]{.3\textwidth}{\raggedleft Période de calage : du \Sexpr{startDate} au \Sexpr{endDate}}
    \parbox[t]{.05\textwidth}{}
    \parbox[t]{.45\textwidth}{\raggedleft Période analysée : du \Sexpr{startDate_analysis} au \Sexpr{endDate_analysis}}
  \end{minipage}
\end{textblock}


%-------------
% table
\vspace{1cm}
<<echo=false,results=tex>>=
  xt <- xtable(analysisTable,
               caption = "",
               digits=c(rep(2,10)))
  align(xt) <- c("p{0.1\\textwidth}|",
                 "|p{0.12\\textwidth}|",
                 "p{0.08\\textwidth}|",
                 "p{0.08\\textwidth}|",
                 "p{0.08\\textwidth}|",
                 "p{0.08\\textwidth}|",
                 "p{0.095\\textwidth}|",
                 "p{0.095\\textwidth}|",
                 "p{0.08\\textwidth}|",
                 "p{0.08\\textwidth}|")

  names(xt) <- c("Date",
                 "Cote du plan d'eau (m NGF)",
                 "Valeur mesurée",
                 "Valeur calculée",
                 "Ecart",
                 "Intervalles de confiance 95\\%",
                 "Intervalles de confiance 99\\%",
                 "Pré-diagnostic 95",
                 "Pré-diagnostic 99")

  bold <- function(x) {paste('{\\textbf{',x,'}}', sep ='')}
  print(xt,
        sanitize.text.function = force,
        include.rownames = FALSE,
        sanitize.colnames.function=bold,
        floating = FALSE,
        hline.after=c(-1),
        size="\\fontsize{9pt}{10pt}\\selectfont",
        tabular.environment = "longtable",
        add.to.row = list(pos = list(0, 0,nrow(xt)),
                            command = c(paste("\\hline \n",
                                              "\\endhead \n",
                                              "\\hline \n",
                                              "\\multicolumn{9}{r}{} \n",
                                              "\\endfoot \n",
                                              "\\endlastfoot \\hline \n",
                                              sep=""),
                                        paste("\\hline \n"),
                                        paste("\\hline \n",  # NEW row
                                              "\\multicolumn{3}{c}{Nb de nouvelles mesures : ", nbNewMeasure,"} & 
                                              \\multicolumn{3}{c}{Nb mesures Ext. int. 95\\% : ", nbExt95,"} & 
                                              \\multicolumn{3}{c}{Nb mesures Ext. int. 99\\% : ", nbExt99,"} \\\\ \n"
                                              )
                                        )
                          )
        )
   
@



\end{document}

