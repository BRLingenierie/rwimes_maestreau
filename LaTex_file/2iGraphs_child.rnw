
%-------------

\begin{center}
\begin{tabular}{ |c|c|c| }
\hline
    \multirow{2}{5 cm}{\centering Code capteur :\ \texttt{\detokenize{\Sexpr{sensorCode_1}}}} & \multirow{2}{8 cm}{\centering \texttt{\detokenize{\Sexpr{sensorData$description}}}} & \multirow{1}{3.7 cm}{\centering \Sexpr{sensorData$sensorType}} \\
    \cline{3-3} &                & \Sexpr{tsUnit}  \\
\hline
\end{tabular}
\end{center}

%-------------
\vspace*{5pt}

%-------------
% interpreted value graph
\begin{minipage}{\linewidth}

<<igraph, fig.width=6.9, fig.height=4.25,echo=FALSE>>=
  print(iGraph)
@

\end{minipage}



